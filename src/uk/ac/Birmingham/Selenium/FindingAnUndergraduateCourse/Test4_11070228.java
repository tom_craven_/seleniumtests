/*
package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse;

*/
/*@Author Tom Craven
 * StuId 11070228
Created 18/11/2016*//*

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class Test4_11070228 extends WebDriverService {

	@BeforeClass
	public static void setupClass() {
		ChromeDriverManager.getInstance().setup();
	}

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "http://www.birmingham.ac.uk/";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test  //Testing the A to Z
	public void test4_11070228() throws Exception {
		selenium.open("/undergraduate/courses/index.aspx");
		selenium.click("//a[contains(text(),'A  to Z')]");
		assertLocation("http://www.birmingham.ac.uk/undergraduate/courses/index.aspx#CourseComplete_AtoZTab", driver);
		selenium.click("link=A");
		selenium.waitForPageToLoad("30000");
		clickElementByHrefOrPartial("/undergraduate/courses/business/accounting-finance.aspx", driver);
		selenium.waitForPageToLoad("30000");
		validateCountLinks(195, selenium);
		validateJSON_LD(driver);
		assertLocation("http://www.birmingham.ac.uk/undergraduate/courses/business/accounting-finance.aspx", driver);
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
		if (driver != null) {
			driver.quit();
		}
	}
}
*/
