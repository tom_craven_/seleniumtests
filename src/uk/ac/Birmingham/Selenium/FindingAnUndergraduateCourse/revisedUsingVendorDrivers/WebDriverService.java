
package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse.revisedUsingVendorDrivers;


import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class WebDriverService {

	WebDriver driver;
	String baseUrl;
	String URL;

	public void clickElementByXpath(String xpath, WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(xpath));
		// Webdriver relies on elements in frame, scroll to view if you need to.
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView()", element);
		element.click();
	}

	public void clickElementByHrefOrPartial(String href, WebDriver driver) {
		String xpath1 = "//a[@href='";
		String xpath3 = "']";
		String xpath2 = href;
		WebElement element = driver.findElement(By.xpath(xpath1 + xpath2 + xpath3));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView()", element);
		element.click();
	}

	public void validateJSON_LD(WebDriver driver) {
		WebElement scriptElement = driver.findElement(By.xpath("html/body/script[3]"));
		String scriptText = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML",
				scriptElement);
		// json data includes special character such as tab space
		String compare = "\n{\n  \"@context\" : \"http://schema.org\",\n  \"@type\" : \"Organization\",\n  \"name\" : \"University of Birmingham\",\n  \"url\" : \"http://www.birmingham.ac.uk\",\n  \"sameAs\" : [\n    \"http://www.facebook.com/unibirmingham\",\n    \"http://twitter.com/unibirmingham\",\n    \"http://plus.google.com/+unibirmingham\",\n    \"http://www.instagram.com/unibirmingham\", \n  	\"https://www.linkedin.com/company/university-of-birmingham\"\n  ]\n}\n";
		assertEquals(compare, scriptText);
	}

	public void assertLocation(String comparator, WebDriver driver) {
		URL = driver.getCurrentUrl();
		assertEquals(URL, comparator);
	}
}

