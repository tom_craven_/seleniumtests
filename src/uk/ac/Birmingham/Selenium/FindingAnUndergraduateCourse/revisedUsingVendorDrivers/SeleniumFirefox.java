package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse.revisedUsingVendorDrivers;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SeleniumFirefox {
	//https://www.seleniumhq.org/docs/03_webdriver.jsp
	private static WebDriver driver = null;

	public static void main(String[] args) {
		   
        System.setProperty("webdriver.gecko.driver","c:\\buildSpace/testing/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://google.com");
        String sTitle = driver.getTitle();
        System.out.println(sTitle);
		
	}
}
