/*
package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse;

*/
/*@Author Tom Craven
 * StuId 11070228
Created 18/11/2016*//*

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class Test2_11070228 extends WebDriverService {

	@BeforeClass
	public static void setupClass() {
		ChromeDriverManager.getInstance().setup();
	}

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "http://www.birmingham.ac.uk/";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test // Testing the search colleges tab
	public void test2_11070228() throws Exception {
		selenium.open("/undergraduate/courses/index.aspx");		
		clickElementByHrefOrPartial("#CourseComplete_CollegesTab", driver);
		assertLocation("http://www.birmingham.ac.uk/undergraduate/courses/index.aspx#CourseComplete_CollegesTab", driver);
		validateJSON_LD(driver);
		selenium.click("css=h2.accordion__title");
		selenium.waitForPageToLoad("30000");
		clickElementByHrefOrPartial(
				"/undergraduate/courses/jointhonours/archaeology-and-ancient-history-and-history.aspx", driver);
		selenium.waitForPageToLoad("30000");
		validateJSON_LD(driver);
		assertLocation(
				"http://www.birmingham.ac.uk/undergraduate/courses/jointhonours/archaeology-and-ancient-history-and-history.aspx",
				driver);
		validateCountLinks(167, selenium);

	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
		if (driver != null) {
			driver.quit();
		}
	}
}
*/
