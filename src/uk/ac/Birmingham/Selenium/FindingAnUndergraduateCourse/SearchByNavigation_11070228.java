/*
package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse.Drafts;


import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import io.github.bonigarcia.wdm.ChromeDriverManager;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;
import static org.apache.commons.lang3.StringUtils.join;

public class SearchByNavigation_11070228 {
	private Selenium selenium;
	private WebDriver driver;
	private String baseUrl;
	private String URL; 
	private Actions actions ;
	
	  @BeforeClass
	public static void setupClass() {
		  ChromeDriverManager.getInstance().setup();
	    }
	  
	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();		
		baseUrl = "http://www.birmingham.ac.uk/";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test
	public void testSearchByNavigation_11070228() throws Exception {
		selenium.open("/index.aspx");
		selenium.click("link=Study");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Undergraduate study");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Search for a course");
		selenium.waitForPageToLoad("30000");
		selenium.type("xpath=.//*[@id='_globalSearchTextBox']", "Software Engineering");
		selenium.click("xpath=.//*[@id='_globalSearchButton']");
		selenium.waitForPageToLoad("30000");
		assertTrue(join(selenium.getAllLinks(), ',').matches("^,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,$"));		
		URL = driver.getCurrentUrl();
		assertEquals(URL, "http://www.birmingham.ac.uk/undergraduate/courses/index.aspx?CurrentTab=Search&CourseKeywords=Software+Engineering");
		WebElement scriptElement = driver.findElement(By.xpath("html/body/script[3]"));
		String scriptText = (String) ((JavascriptExecutor)driver).executeScript(
		    "return arguments[0].innerHTML", scriptElement);
		String compare = "\n{\n  \"@context\" : \"http://schema.org\",\n  \"@type\" : \"Organization\",\n  \"name\" : \"University of Birmingham\",\n  \"url\" : \"http://www.birmingham.ac.uk\",\n  \"sameAs\" : [\n    \"http://www.facebook.com/unibirmingham\",\n    \"http://twitter.com/unibirmingham\",\n    \"http://plus.google.com/+unibirmingham\",\n    \"http://www.instagram.com/unibirmingham\", \n  	\"https://www.linkedin.com/company/university-of-birmingham\"\n  ]\n}\n";			
		assertEquals(compare, scriptText);						
		selenium.click("link=Computer Science/Software Engineering MEng");
		selenium.waitForPageToLoad("30000");
		assertTrue(selenium.getTitle().matches("^Undergraduate degree course in Computer Science/Software Engineering MEng - GG46 - Computer Science, University of Birmingham - University of Birmingham$"));
		assertEquals(1, selenium.getXpathCount(".//*[@id='form1']/main/article/div[1]/div/aside/div[1]/div[2]/dl[1]/dd[3]/a"));
		assertTrue(selenium.getLocation().matches("^http://www\\.birmingham\\.ac\\.uk/undergraduate/courses/computer-science/computer-science-software-engineering\\.aspx$"));
		assertTrue(selenium.isElementPresent("xpath=(//button[@type='button'])[2]"));
		scriptElement = driver.findElement(By.xpath("html/body/script[3]"));
		scriptText = (String) ((JavascriptExecutor)driver).executeScript(
			    "return arguments[0].innerHTML", scriptElement);
		compare = "\n{\n  \"@context\" : \"http://schema.org\",\n  \"@type\" : \"Organization\",\n  \"name\" : \"University of Birmingham\",\n  \"url\" : \"http://www.birmingham.ac.uk\",\n  \"sameAs\" : [\n    \"http://www.facebook.com/unibirmingham\",\n    \"http://twitter.com/unibirmingham\",\n    \"http://plus.google.com/+unibirmingham\",\n    \"http://www.instagram.com/unibirmingham\", \n  	\"https://www.linkedin.com/company/university-of-birmingham\"\n  ]\n}\n";						
		assertEquals(compare, scriptText);	
		assertTrue(join(selenium.getAllLinks(), ',').matches("^,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,CMS_A0fd192eb53ac43fda21c7d6c4464d799,,,,,,,,,,,,CMS_A80885023a3f847f689a7e70878665e1e,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,$"));
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
		if (driver != null) {
            driver.quit();
        }
	}
}
*/
