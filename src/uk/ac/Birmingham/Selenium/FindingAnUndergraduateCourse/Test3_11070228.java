/*
package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse;

*/
/*@Author Tom Craven
 * StuId 11070228
Created 18/11/2016*//*

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class Test3_11070228 extends WebDriverService {

	@BeforeClass
	public static void setupClass() {
		ChromeDriverManager.getInstance().setup();
	}

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "http://www.birmingham.ac.uk/";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test // testing the undergraduate global search
	public void test3_11070228() throws Exception {
		selenium.open("/undergraduate/courses/index.aspx");
		validateJSON_LD(driver);
		validateCountLinks(532, selenium);
		selenium.type("id=_globalSearchTextBox", "business studies");
		selenium.click("id=_globalSearchButton");
		selenium.waitForPageToLoad("30000");
		validateCountLinks(556, selenium);
		assertLocation(
				"http://www.birmingham.ac.uk/undergraduate/courses/index.aspx?CurrentTab=Search&CourseKeywords=business+studies",
				driver);
		clickElementByHrefOrPartial("/undergraduate/courses/business/bus-mgnt-year-in-industry.aspx", driver);
		selenium.waitForPageToLoad("30000");
		validateJSON_LD(driver);
		assertLocation("http://www.birmingham.ac.uk/undergraduate/courses/business/bus-mgnt-year-in-industry.aspx",
				driver);
		validateCountLinks(226, selenium);
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
		if (driver != null) {
			driver.quit();
		}
	}
}
*/
