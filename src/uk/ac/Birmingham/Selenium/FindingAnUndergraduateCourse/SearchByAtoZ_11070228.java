/*
package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse.Drafts;

import static org.apache.commons.lang3.StringUtils.join;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class SearchByAtoZ_11070228 {
	private SeleniumChrome selenium;
	private WebDriver driver;
	private String baseUrl;
	private String URL; 
	private Actions actions ;
	  @BeforeClass
	public static void setupClass() {
		  System.setProperty("webdriver.chrome.driver", "c:\\buildSpace/chromedriver_win32/chromedriver.exe");
	    }
	  
	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();		
		baseUrl = "http://www.birmingham.ac.uk/";
		selenium = new WebDriverBackedSelenium();

	}

	@Test
	public void testSearchByAtoZ_11070228() throws Exception {
		selenium.open("/index.aspx");
		selenium.click("link=Study");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Undergraduate study");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Search for a course");
		selenium.waitForPageToLoad("30000");
		selenium.click("//a[contains(text(),'A  to Z')]");
		selenium.click("link=S");
		selenium.waitForPageToLoad("30000");
		assertEquals(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,", join(selenium.getAllLinks(), ','));
		assertTrue(selenium.getLocation().matches("^http://www\\.birmingham\\.ac\\.uk/undergraduate/courses/index\\.aspx[\\s\\S]CurrentTab=AtoZ&CourseComplete_AtoZ_AtoZLetter=S$"));
		WebElement scriptElement = driver.findElement(By.xpath("html/body/script[3]"));
		String scriptText = (String) ((JavascriptExecutor)driver).executeScript(
		    "return arguments[0].innerHTML", scriptElement);
		String compare = "\n{\n  \"@context\" : \"http://schema.org\",\n  \"@type\" : \"Organization\",\n  \"name\" : \"University of Birmingham\",\n  \"url\" : \"http://www.birmingham.ac.uk\",\n  \"sameAs\" : [\n    \"http://www.facebook.com/unibirmingham\",\n    \"http://twitter.com/unibirmingham\",\n    \"http://plus.google.com/+unibirmingham\",\n    \"http://www.instagram.com/unibirmingham\", \n  	\"https://www.linkedin.com/company/university-of-birmingham\"\n  ]\n}\n";			
		assertEquals(compare, scriptText);
		
		WebElement element = driver.findElement(By.xpath("//a[@href='/undergraduate/courses/jointhonours/archaeology-and-ancient-history-and-history.aspx']"));				
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].scrollIntoView()", element); 
		element.click();			
		selenium.waitForPageToLoad("30000");		
		assertEquals("Undergraduate degree course in Computer Science/Software Engineering MEng - GG46 - Computer Science, University of Birmingham - University of Birmingham", selenium.getTitle());
		assertEquals(1, selenium.getXpathCount(".//*[@id='form1']/main/article/div[1]/div/aside/div[1]/div[2]/dl[1]/dd[3]/a"));
		assertEquals("http://www.birmingham.ac.uk/undergraduate/courses/computer-science/computer-science-software-engineering.aspx", selenium.getLocation());
		assertTrue(selenium.isElementPresent("xpath=(//button[@type='button'])[2]"));
		scriptElement = driver.findElement(By.xpath("html/body/script[3]"));
		scriptText = (String) ((JavascriptExecutor)driver).executeScript(
			    "return arguments[0].innerHTML", scriptElement);
		compare = "\n{\n  \"@context\" : \"http://schema.org\",\n  \"@type\" : \"Organization\",\n  \"name\" : \"University of Birmingham\",\n  \"url\" : \"http://www.birmingham.ac.uk\",\n  \"sameAs\" : [\n    \"http://www.facebook.com/unibirmingham\",\n    \"http://twitter.com/unibirmingham\",\n    \"http://plus.google.com/+unibirmingham\",\n    \"http://www.instagram.com/unibirmingham\", \n  	\"https://www.linkedin.com/company/university-of-birmingham\"\n  ]\n}\n";						
		assertEquals(compare, scriptText);	
		assertEquals(",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,CMS_A0fd192eb53ac43fda21c7d6c4464d799,,,,,,,,,,,,CMS_A80885023a3f847f689a7e70878665e1e,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,", join(selenium.getAllLinks(), ','));
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
		if (driver != null) {
            driver.quit();
        }
	}
}
*/
