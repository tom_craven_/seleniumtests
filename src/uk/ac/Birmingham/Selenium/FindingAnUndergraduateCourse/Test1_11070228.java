/*
package uk.ac.Birmingham.Selenium.FindingAnUndergraduateCourse;

*/
/*@Author Tom Craven
 * StuId 11070228
Created 18/11/2016*//*

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class Test1_11070228 extends WebDriverService {

	@BeforeClass
	public static void setupClass() {


	}

	@Before
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "http://www.birmingham.ac.uk/";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test
	public void test1_11070228() throws Exception {
		selenium.open("/index.aspx");
		validateJSON_LD(driver);
		clickElementByHrefOrPartial("/study/index.aspx", driver);
		selenium.waitForPageToLoad("30000");
		assertLocation("http://www.birmingham.ac.uk/study/index.aspx", driver);
		validateCountLinks(122, selenium);
		clickElementByHrefOrPartial("http://www.birmingham.ac.uk/undergraduate/index.aspx", driver);
		selenium.waitForPageToLoad("30000");
		validateJSON_LD(driver);
		selenium.click("css=button.search-toggle.js-search-toggle");
		selenium.type("id=_globalSearchTextBox", "computer science");
		validateCountLinks(134, selenium);
		selenium.click("id=_globalSearchButton");
		selenium.waitForPageToLoad("30000");
		validateJSON_LD(driver);
		clickElementByHrefOrPartial("/undergraduate/courses/computer-science/computer-science.aspx", driver);
		selenium.waitForPageToLoad("30000");
		assertLocation("http://www.birmingham.ac.uk/undergraduate/courses/computer-science/computer-science.aspx", driver);
		validateCountLinks(161, selenium);
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
		if (driver != null) {
			driver.quit();
		}
	}
}
*/
